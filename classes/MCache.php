<?php
namespace classes;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once ( dirname(__FILE__). '/LiteMemcache.class.php');
/**
 * Description of MCache
 *
 * @author antonio
 */
class MCache extends \LiteMemcache{
    //put your code here
    
    private static $__i;
    /**
     * @return MCache Description
     */
    public static function _i($host='127.0.0.1', $port='11211'){
        if(!self::$__i){
            self::$__i = new MCache( $host.':'.$port );
        }
        return self::$__i;
    }
    
    public function set( $key, $value, $exptime = 0, $flags = 0 ){
        if(!is_string($value) || !is_int($value) || !is_float($value)){
            $value=  json_encode($value);
        }
        return parent::set($key, $value, $exptime, $flags);
	}
    
    public function get( $key, $ext = false )
	{
		$value=  parent::get($key, $ext);
        if($value && (!is_string($value) || !is_int($value) || !is_float($value) ) ){
            $value= json_decode($value);
        }
        return $value;
	}
}
