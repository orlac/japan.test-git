<?php
namespace classes;

class Route{
    
    public static $defaultController = 'index';
    public static $defaultAction = 'index';
    
    public static function run(){
        //print_r($_SERVER);
        $url = $_SERVER['REQUEST_URI'];
        $url = preg_replace('/^\//', '', $url);
        $data = parse_url($url); 
        $uri = explode( '/', $data['path'] );
        $c = array_shift($uri);
        $c = ($c)? $c : self::$defaultController;
        
        $a = array_shift($uri);
        $a = ($a)? $a : self::$defaultAction;
        if(!file_exists('controllers/'.$c.'.php')){        
            header("HTTP/1.0 404 Not Found - Archive Empty");
        }else{
            $c = '\\controllers\\'.$c;
            $c = new $c();
            
            if(!method_exists($c, $a)){
                array_unshift( $uri, $a );
                $a = self::$defaultAction;
            }
            
            if(!method_exists($c, $a)){
                //throw new Exception('action '.$aName.' not found');
                header("HTTP/1.0 404 Not Found - Archive Empty");
                
            }else{
                $r = new \ReflectionMethod($c, $a);
                $r->invokeArgs($c, $uri);
                //$r->invoke($c);
                //$params = $r->getParameters();        
            }
        }
    }
}