<?php
namespace controllers;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of index
 *
 * @author antonio
 */
class index extends controller{
    //put your code here
    
    public function index(){
            $this->render('index/index' );    
    }
    
    public function rules() {
        return array(
            'signIn'=>'noAuth',
            'signUp'=>'noAuth',
            'confirm'=>'noAuth',
            'logout'=>'isAuth',
        );
    }
    
    public function signIn(){
        $this->checkRule(__FUNCTION__);
        $user=new \models\mySql\User();
        if($_POST && isset($_POST['user'])){
            $user->load( (array)$_POST['user'] );
            if(\models\mySql\User::login($user)){
                header('location: /');
            }
        }
        $this->render('index/signIn', array('user'=>$user) );  
    }
    
    public function signUp(){
        $this->checkRule(__FUNCTION__);
        $user=new \models\mySql\User();
        if($_POST && isset($_POST['user'])){
            $user->load( (array)$_POST['user'] );
            if($user->validate() && \models\mySql\User::register($user) ){
                header('location: /');
            }
        }
        $this->render('index/signUp', array('user'=>$user) );    
    }
    
    public function logout(){
        $this->checkRule(__FUNCTION__);
        \models\mySql\User::logout();
        header('location: /');
    }
    
    public function confirm(){
        $this->checkRule(__FUNCTION__);
        if($_GET && isset($_GET['code'])){
            $code=$_GET['code'];
            $error=true;
            if(\models\mySql\User::confirm($code)){
                $error=false;
            }
            $this->render('index/confirm', array('error'=>$error) );    
        }else{
            header('location: /');
        }
    }
    
}
