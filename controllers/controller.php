<?php
namespace controllers;

class controller{
    
    
    public static $viewDir = 'views',
           $layout = 'index' ;
    
    public function rules() {
        return array();
    }
    
    public function checkRule($action) {
        $_rules = $this->rules();
        if(isset($_rules[$action])){
            if(!$this->{$_rules[$action]}()){
                header('HTTP/1.0 403 Forbidden');
                exit;
            }
        }
    }
    
    public function noAuth() {
        return (boolean)!\models\mySql\User::getCurrent();
    }
    
    public function isAuth() {
        return (boolean)\models\mySql\User::getCurrent();
    }
    
    public function render( $view, $data = array() ){
        
        ob_start();
        extract( $data );
        require_once ( self::$viewDir.'/'.$view.'.php' );
        $content = ob_get_contents();
        ob_end_clean();
        
        require_once ( self::$viewDir.'/layouts/'.self::$layout.'.php' );
    }    
    
}
?>