<?php
require 'Loader.php';
Loader::register();

define ('BASE_DIR', dirname(__FILE__) );


session_start();

\models\Model::$driver = \models\Model::mySql;
/**
 * connect to mysql user:root, password:
 */
$link = new PDO('mysql:host=127.0.0.1;dbname=jtl', 'root', '');
\models\Model::$dbConnect = $link;

\controllers\controller::$viewDir = BASE_DIR.'/theme/def/views';

\classes\Route::$defaultController = 'index';
\classes\Route::$defaultAction = 'index';
/**
 * connect to memcache host:127.0.0.1, port:11211:
 */
classes\MCache::_i('127.0.0.1', '11211');
\classes\Route::run();
