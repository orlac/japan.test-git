<?php
namespace models;

interface Storage{
    
    public function save();
    
    public function get( $where = array() );
    
    public function toArray( );
    
    public function rules();
     
}