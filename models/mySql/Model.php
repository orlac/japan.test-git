<?php
namespace models\mySql;

class Model{
    
    protected $table, $errors;
    
    public function load(array $data){
        if(count($data) > 0){
            $rules=  $this->rules();
            foreach( $data as $key => $val ){
                if(isset($rules[$key])){
                    $this->$key( $val );
                }
            }
        }
    }

    
    protected function _save($sql, $params=array()){
        
        if($this->validate()){
            if( $this->query( $sql, $params ) !== false ){
                $this->id = ( $this->id > 0 )? $this->id : $this->lastInsertId();
                
                return true;
            }
        }
        return false;
    }
    
    protected function _count( $sql ){
        $sqlCount = str_replace(':fields', 'count(*) as c', $sql );
        $sqlCount = str_replace(':limit', ' limit 1 ', $sqlCount );
        $result =  array_shift( $this->query( $sqlCount ) );// mysql_fetch_row($r);
        return $result['c'];
    }
    
    protected function pagerSetup($sql, \classes\Pager $pager = null){
        $limit = ($pager !== null)? 'limit '. $pager->current*$pager->size.', '.$pager->size : '';
        return str_replace(':limit', $limit, $sql);
    }
    
    protected function _addFields($sql, array $fields){
        return str_replace(':fields', implode(', ', $fields), $sql);
    }
    
    protected function query($sql, $params = array()){
        $res = \models\Model::$dbConnect->prepare($sql);
        if($params){
            $ex = $res->execute($params);
        }else{
            $ex = $res->execute();
        }
        if( !$ex ){
            $e = \models\Model::$dbConnect->errorInfo();
            $this->setError( $e[2].' sql: '.$sql, 'sys' );
            return false;
        }
        $data = array();
        while ($row = $res->fetch(\PDO::FETCH_ASSOC)){ 
            $data[] = $row;
        }
        return $data;
    }
    
    public function clearErrors($attribute = null){
        if( $attribute !== null  && isset($this->errors[$attribute]) ){
            unset( $this->errors[$attribute] );
        }else{
            $this->errors = array();
        }
    }
    
    public function setError($error, $attribute = null){
        if( $attribute !== null ){
            $this->errors[$attribute] = $error;
        }else{
            $this->errors[] = $error;
        }
    }
    
    public function getErrors( $attribute = null ){
        if( $attribute !== null){
            return (isset($this->errors[$attribute]))? $this->errors[$attribute] : null;
        }else{
            return $this->errors;
        }
    }
    
    public function find(){
        return array_shift($this->get());
    }
    
    public function __call( $name, $args ){
        if( is_array($args) && count($args) > 0 ){
            //set
            if( method_exists( $this, '_set_'.$name) ){
                call_user_func_array(array($this, '_set_'.$name), $args);
            }else{
                if( property_exists($this, $name ) ){
                    $this->$name = array_shift($args);    
                }
            }
        }else{
            //get
            if( method_exists($this, '_get_'.$name) ){
                $f = '_get_'.$name;
                return $this->$f();
            }else{
                return $this->$name;
            }
        }
    }
    
    public function lastInsertId(){
        return \models\Model::$dbConnect->lastInsertId();
    }
    
    public function validate( ){
        $rules=  $this->rules();
        foreach ($rules as $name => $func){
            if($func !== false){
                $this->{$func}();
            }
        }
        if($this->getErrors()){
            return false;
        }
        return true;
    }
}