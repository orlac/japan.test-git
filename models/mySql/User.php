<?php
namespace models\mySql;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of User
 *
 * @author antonio
 */
class User extends Model implements \models\Storage{
    //put your code here
    
    public
        $id,
        $email,
        $name,
        $pswd,
        $createdOn;
    
    protected $table = 'user';
    
    public function toArray(){
        return array(
            'id' => $this->id(),
            'email' => $this->email(),
            'name' => $this->name(),
            //'pswd' => $this->pswd(),
            'createdOn' => $this->createdOn(),
        );
    }
    
    public function rules() {
        return array(
            'email'=>'checkEmail',
            'name'=>'checkName',
            'pswd'=>false,
            'id'=>false,
        );
    }
    
    public function checkEmail() {
        if( !$this->email ){
            $this->setError( 'введите email', 'email' );
        }
        if(!filter_var($this->email, FILTER_VALIDATE_EMAIL)){
            $this->setError( 'неверный email', 'email' );
        }else{
            //check email;
            $sql='select count(*) as cnt from '.$this->table.' where email=:email';
            $data = $this->query($sql, array(':email'=>  $this->email));
            if( (int) $data[0]['cnt'] > 0){
                $this->setError( 'email уже занят', 'email' );
            }
        }
    }
    
    public function checkName() {
        if( !$this->name ){
            $this->setError( 'введите имя', 'name' );
        }
    }
    
    public function save(){
        
        if($this->id > 0){
            $sql = "update ".$this->table." set email = :email, name = :name where id = :id" ;
            $params = array(':email' => $this->email, ':name' => $this->name, ':id' => (int)$this->id);
        }else{
            $sql = "Insert into ".$this->table." (email , name, pswd, createdOn) "
                . "values ( :email , :name, :pswd, now() )";
            $params = array(':email' => $this->email, ':name' => $this->name, ':pswd' => $this->pswd);
        }
        return parent::_save($sql, $params);
    }
    
    public function get($where = array()){
        $fields = array( '*' );
        $sql = "select :fields from ".$this->table." :where ";
        
        //condition
        $where = array();
        $params = array();
        if( $this->id !== null ){
            $where[] = ' id = :id ' ;
            $params[':id'] = $this->id;
        }
        if( $this->email !== null ){
            $where[] = " email = :email " ;
            $params[':email'] = $this->email;
        }
        if( $this->name !== null ){
            $where[] = " name = :name " ;
            $params[':name'] = $this->name;
        }
        if( $this->pswd !== null ){
            $where[] = " pswd = :pswd " ;
            $params[':pswd'] = $this->pswd;
        }
        $where = ( count($where) > 0 )? ' where '. implode(' and ', $where) : '';
        $sql = str_replace(':where', $where, $sql);
        $sql = $this->_addFields($sql, $fields );
        $data = $this->query($sql, $params);
        if(count($data) > 0){
            foreach( $data as $key => $val ){
                    $obj = new User();
                    $obj->load( $val );
                    $data[$key] = $obj;
            }
        }  
        return $data;
    }
    
    public static function getCurrent(){
        if(isset($_SESSION['_uid'])){
            $model=new User();
            $model->id=(int)$_SESSION['_uid'];
            return $model->find();
        }
    }
    
    public static function register(User $user){
        $user->pswd=self::generatePswd();
        $code=self::generateCode();
        self::preSave($user, $code);
        $link='http://'.$_SERVER['HTTP_HOST'].'/index/confirm?code='.$code;
        mail($user->email, 'confirm code', 'go to page '.$link);
        return true;
    }
    
    public static function confirm($code){
        $objUser=self::getPreSaved($code);
        if($objUser){
            $pswd=self::generatePswd();
            $user=new User();
            $user->email=$objUser->email;
            $user->name=$objUser->name;
            $user->pswd= md5($pswd);
            if($user->save()){
                mail($user->email, 'pswd', 'login: '.$user->email.'; pswd: '. $pswd);
                \classes\MCache::_i()->del($code);
                return true;
            }else{
                return false;
            }
        }
        return false;
    }
    
    public static function login(User &$user){
        $_user=clone $user;
        $_user->pswd=md5($user->pswd);
        $model=$_user->find();
        if(!$model){
            $user->setError('пароль или e-mail ненайдены');
            return false;
        }else{
            $_SESSION['_uid']=$model->id;
            return true;
        }
    }
    
    public static function logout(){
        if(isset($_SESSION['_uid'])){
            unset($_SESSION['_uid']);
        }
    }
    
    
    private static function preSave(User $user, $code) {
        \classes\MCache::_i()->set($code, $user->toArray(), 600);
    }
    
    private static function getPreSaved($code) {
        $objUser = \classes\MCache::_i()->get($code);
        if(!empty($objUser)){
            return $objUser;
        }
    }
    
    
    
    private static function generatePswd() {
        $confirmCode='';
        for ($i = 0; $i < 6; $i++) {
            $confirmCode .= rand(0, 9);
        }
        return (string)$confirmCode;
    }
    
    private static function generateCode() {
        $confirmCode='';
        for ($i = 0; $i < 6; $i++) {
            $confirmCode .= rand(0, 9);
        }
        return md5((string)$confirmCode);
    }
}
