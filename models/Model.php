<?php
namespace models;

class Model{
    
    const mySql = 'mySql';
    const noSql = 'noSql';
    
    public static $driver = self::mySql;
    public static $dbConnect;
    
    public static function factory( $model ){
        
        $m = 'models\\'.self::$driver.'\\'.$model;
        return new $m( );
    }
    
}
?>