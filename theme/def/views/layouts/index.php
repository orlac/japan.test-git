<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<!--bootstarp-->
<link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" />
</head>
<body>
    <div class="panel panel-default">
        <div class="panel-heading">
            <ul class="nav nav-tabs" role="tablist">
                <li class="<?=( ($_SERVER['REQUEST_URI']=='/')?'active' : '' )?>"><a href="/">Home</a></li>
                <?php if(!models\mySql\User::getCurrent()){ ?>
                    <li class="<?=( ($_SERVER['REQUEST_URI']=='/index/signUp')?'active' : '' )?>"><a href="/index/signUp">Sign up</a></li>
                    <li class="<?=( ($_SERVER['REQUEST_URI']=='/index/signIn')?'active' : '' )?>"><a href="/index/signIn">Sign in</a></li>
                <? }else{ ?>
                    <li class="<?=( ($_SERVER['REQUEST_URI']=='/index/logout')?'active' : '' )?>"><a href="/index/logout">Logout</a></li>
                <? } ?>
              </ul>
        </div>
        <div class="panel-body">
            <?php echo $content; ?> 
        </div>
    </div>
</body>
</html>

</html>