<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/* @var $user models\mySql\User */
?>
<form method="post">
    <div class="panel panel-default" style="width: 500px;margin: 0 auto;">
        <div class="panel-body">
            <?php if($errors=$user->getErrors()){ 
                foreach ($errors as $err){ ?>
                    <div class="alert alert-danger" role="alert"><?=$err;?></div>
                <?}
            }?>

                <div class="input-group">
                    <span class="input-group-addon">e-mail</span>
                    <input type="text" class="form-control" name="user[email]" value="<?=$user->email?>">
                </div>
                <br>
                <div class="input-group">
                    <span class="input-group-addon">password</span>
                    <input type="password" class="form-control" name="user[pswd]"  value="<?=$user->pswd?>">
                </div>
        </div>
        <div class="panel-footer"><input type="submit" value="sign up"/></div>
    </div>
</form>